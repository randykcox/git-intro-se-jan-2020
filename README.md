### Git Demo Readme

Git commands:

* git init
* git status
* git add _______
* git commit -m "________"
* git restore ______
* git diff filename   (shows the diff for a file since the last commit)
* git diff 123abc 345def (show the diff between two commit ids)
* git remote add origin __________
* git push -u origin --all
* git push origin master
* git push

This change is local